<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>API UPLOAD IMGUR</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="copy.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>

    <style>
        .button-copy {
            display: inline-flex;
            padding-left: 15px;
        }

        .copy--status--bubble {
            line-height: 35px;
            padding-left: 10px;
        }
        .alert-success {
            color: #3c763d;
            background-color: #dff0d8;
            border-color: #d6e9c6;
            overflow: hidden;
            text-align: center;
        }
        #loading-indicator {
            margin: 0 auto;
        }
        .logo img {
            height: 155px;
        }

        .logo {
            text-align: center;
            margin: 10px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="logo">
        <img src="https://s.imgur.com/images/logo-1200-630.jpg" alt="">
    </div>
    <div class="panel panel-success">
        <div class="panel-body">

            <form action="" method="POST" role="form">
                <legend>Upload</legend>

                <div class="form-group">
                    <label for="">Chọn file</label>
                    <input id="file" type="file" name="sortpic" required="" />
                </div>
                <div class="form-group">
                    <button id="upload" class="btn btn-primary">Upload</button>
                </div>
            </form>
            <div class="content  alert alert-success">
            <img src="loading.svg" id="loading-indicator" style="display:none" />
            <div class="status">
            </div>

            </div>
        </div>
    </div>
</div>
<script>

    $('#upload').on('click', function(e) {
        e.preventDefault();
        var file_data = $('#file').prop('files')[0];

        var type = file_data.type;

        var match= ["image/gif","image/png","image/jpg","image/jpeg"];

        //alert(type );
        if(type == match[0] || type == match[1] || type == match[2] || type == match[3])
        {

            var form_data = new FormData();

            form_data.append('file', file_data);
            $('#loading-indicator').show();

            $.ajax({
                url: 'upload.php',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(res){

                    $('.status').html(res);
                    $('#file').val('');
                    $('#loading-indicator').hide();
                }

            });

        } else{
            $('.status').text('Chỉ được upload file ảnh');
            $('#file').val('');
        }
        return false;
    });

</script>
</body>
</html>
